Link Budget Analysis
====================

This repository contains files and scripts related to the link budget analysis on the AcubeSAT mission. Analysis on UHF and S band antennas from the proposal till the Critical Design Review phase is included. 

#### ``/CDR``
Link budget analysis that was conducted for the CDR.

#### ``/Proposal``
Link budget analysis that was conducted for the proposal.

#### ``/SSO``
Link budget analysis for Sun-synchronous orbits with an altitude of 400, 450, 500 and 550 km for UHF Uplink, UHF Downlink and S Band Downlink. This analysis is not used in the final project. 
