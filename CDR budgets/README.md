## CDR Link Budget Analysis

This folder contains the link budget analysis on the AcubeSAT UHF and S band antennas. The excel sheets that were used for the anaysis are included.

* ``S_band_downlink``/``UHF_link``<br />
 &nbsp;   &nbsp;  Excel sheets for calculating the budget on the S band and UHF 
 respectively.


