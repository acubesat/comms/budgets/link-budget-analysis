import numpy as np
import matplotlib.pyplot as plt
#import matplotlib
import matplotlib.patches as patches
import matplotlib.pyplot as pl

def find_nearest(array, value):
    array = np.asarray(array)
    idx = (np.abs(array - value)).argmin()
    return idx

plt.rcParams["figure.figsize"] = (17,10)

bitrates = np.arange(30, 301, dtype=object)

margins400 = np.loadtxt('link_margin_SSO_400.txt', delimiter=' ')
margins450 = np.loadtxt('link_margin_SSO_450.txt', delimiter=' ')
margins500 = np.loadtxt('link_margin_SSO_500.txt', delimiter=' ')
margins550 = np.loadtxt('link_margin_SSO_550.txt', delimiter=' ')

plt.title('$\mathrm{SSO}\ $|$\ \mathrm{Link\ Budget\ Analysis \ (S \ Band \ Downlink)}$', fontsize=30, y=1.01)
plt.xlabel('$\mathrm{Data \ rate \ (kbps)}$', fontsize=20, y=0.01)
plt.ylabel('$\mathrm{System \ Link \ Margin \ (dB)}$', fontsize=20, x=1.01)

plt.axhline(y=10, color='brown', linestyle='solid', linewidth=2, zorder=5)

plt.plot(bitrates, margins400, c='#1f77b4', label="$\mathrm{400 \ km}$", linewidth=2, zorder=7)
plt.plot(bitrates, margins450, c='#2ca02c', label="$\mathrm{450 \ km}$", linewidth=2, zorder=7)
plt.plot(bitrates, margins500, c='#ff7f0e', label="$\mathrm{500 \ km}$", linewidth=2, zorder=7)
plt.plot(bitrates, margins550, c='#d62728', label="$\mathrm{550 \ km}$", linewidth=2, zorder=7)

#plt.scatter([bitrates[(find_nearest(margins400, 10))] ],[10], c='#1f77b4', s=10**2, zorder=10)
plt.scatter([bitrates[(find_nearest(margins450, 10))] ],[10], c='#2ca02c', s=10**2, zorder=10)
plt.scatter([bitrates[(find_nearest(margins500, 10))] ],[10], c='#ff7f0e', s=10**2, zorder=10)
plt.scatter([bitrates[(find_nearest(margins550, 10))] ],[10], c='#d62728', s=10**2, zorder=10)

plt.scatter(margins550[bitrates[(find_nearest(margins550, 140))] ],[10], c='#d62728', s=10**2, zorder=10)

plt.scatter([140], margins550[140-30], c='yellow', s=10**2, zorder=10)

plt.axhline(y=0.01+margins550[140-30], color='yellow', linestyle='--', linewidth=2, xmin=0, xmax=0.4)

plt.text(31,11.6,'$'+str(margins550[140-30])[0:5]+' \ \mathrm{dB}$', fontsize=18)

plt.text((bitrates[(find_nearest(margins550, 10))])-4,10.3,'$'+str((bitrates[(find_nearest(margins550, 10))]))+'$', fontsize=18)
plt.text((bitrates[(find_nearest(margins500, 10))])-4,10.3,'$'+str((bitrates[(find_nearest(margins500, 10))]))+'$', fontsize=18)
plt.text((bitrates[(find_nearest(margins450, 10))])-4,10.3,'$'+str((bitrates[(find_nearest(margins450, 10))]))+'$', fontsize=18)

plt.text(140-4,11.73, '$'+str(140)+'$', fontsize=18)
plt.text(146,11.35, '$\mathrm{16\% \ downtime}$', fontsize=16, zorder=20)
#plt.text(71,13.75, '$\mathrm{(320 \ days)}$', fontsize=16)

plt.text(290.1,22.15,'$\mathrm{v2.0}$', fontsize=16)

plt.text(273.9,21.4,'$\mathrm{Altitudes}$', fontsize=18)
plt.text(32.7,10.25,'$\mathrm{Reliable \ Link}$', fontsize=20)
plt.text(32.7,9.33,'$\mathrm{Unreliable \ Link}$', fontsize=20)
plt.fill_between([30,300], [22,22], facecolor='green', alpha=0.2)
plt.fill_between([30,300], [10,10], facecolor='red', alpha=0.2)
plt.xlim(30,300)
plt.ylim(6,22)
plt.legend(bbox_to_anchor=(1,0.965), loc="upper right")



plt.grid()


plt.savefig('sso.png', bbox_inches='tight')
